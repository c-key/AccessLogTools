﻿using System;
using System.Reflection;

namespace AccessLogParser.Utils
{
    public static class Version
    {
        public static string GetInfoVersion()
        {
            return GetEntryAssemblyAttribute<AssemblyInformationalVersionAttribute>()?.InformationalVersion;
        }

        public static string GetFileVersion()
        {
            return GetEntryAssemblyAttribute<AssemblyFileVersionAttribute>()?.Version;
        }

        public static string GetAssemblyVersion()
        {
            return GetEntryAssembly()?.GetName().Version?.ToString();
        }

        private static Assembly GetEntryAssembly()
        {
            return Assembly.GetEntryAssembly();
        }

        private static T GetEntryAssemblyAttribute<T>() where T : Attribute
        {
            return GetEntryAssembly()?.GetCustomAttribute<T>();
        }
    }
}