﻿using System.Text;
using AccessLogParser.Services.Abstractions;

namespace AccessLogParser.Services
{
    public class Version : IVersion
    {
        public string GetCompleteVersionInfo()
        {
            return (new StringBuilder())
                .Append("Version: ").AppendLine(Utils.Version.GetInfoVersion())
                .Append("File: ").AppendLine(Utils.Version.GetFileVersion())
                .Append("Assembly: ").Append(Utils.Version.GetAssemblyVersion())
                .ToString();
        }
    }
}