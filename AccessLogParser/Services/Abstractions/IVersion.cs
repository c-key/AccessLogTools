﻿namespace AccessLogParser.Services.Abstractions
{
    public interface IVersion
    {
        string GetCompleteVersionInfo();
    }
}