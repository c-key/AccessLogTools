﻿namespace AccessLogParser.Services.Abstractions
{
    public interface IParser
    {
        void CheckConfiguration();

        void Parse();
    }
}