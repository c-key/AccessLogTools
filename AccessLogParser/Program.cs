﻿using McMaster.Extensions.CommandLineUtils;

namespace AccessLogParser
{
    class Program
    {
        static int Main(string[] args) => CommandLineApplication.Execute<Executor>(args);
    }
}