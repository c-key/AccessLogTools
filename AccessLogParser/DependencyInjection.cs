﻿using System;
using AccessLogParser.Services;
using AccessLogParser.Services.Abstractions;
using Microsoft.Extensions.DependencyInjection;
using Version = AccessLogParser.Services.Version;

namespace AccessLogParser
{
    static class DependencyInjection
    {
        public static IServiceProvider Init()
        {
            var serviceProvider = new ServiceCollection()
                .AddScoped<IVersion, Version>()
                .AddScoped<IParser, Parser>()
                .BuildServiceProvider();

            return serviceProvider;
        }
    }
}