﻿using System;
using AccessLogParser.Services.Abstractions;
using McMaster.Extensions.CommandLineUtils;
using Microsoft.Extensions.DependencyInjection;

namespace AccessLogParser
{
    [HelpOption]
    public class Executor
    {
        [Option(ShortName = "v", Description = "Shows the version")]
        public bool Version { get; set; }
        
        [Option(ShortName = "c", LongName = "config", Description = "Path to configuration file. Relative or absolute.")]
        public string Configuration { get; set; }
        
        private void OnExecute()
        {
            var serviceProvider = DependencyInjection.Init();

            if (!DoExecution(serviceProvider))
            {
                Console.Out.WriteLine("Use -? or -h or --help");                
            }
            
            if (serviceProvider is IDisposable)
            {
                ((IDisposable) serviceProvider).Dispose();
            }
        }

        private bool DoExecution(IServiceProvider serviceProvider)
        {
            if (Version)
            {
                Console.Out.WriteLine(serviceProvider.GetService<IVersion>()?.GetCompleteVersionInfo());
                return true;
            }

            if (!string.IsNullOrEmpty(Configuration))
            {
                Console.Out.WriteLine($"Do work with Configuration {Configuration}");
                var parser = serviceProvider.GetService<IParser>();
                parser?.CheckConfiguration();
                parser?.Parse();
                return true;
            }

            return false;
        }
    }
}